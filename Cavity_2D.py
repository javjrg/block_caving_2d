from parameters import *

print ("-------------------------------------------------------------------")
print ("                       The Code Has Started.                       ")
print ("-------------------------------------------------------------------")

parameters.parse()   # read paramaters from command line
# set some dolfin specific parameters
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs"
parameters["allow_extrapolation"] = True


#----------------------------------------------------------------
# The minimization procedure requires parameters to get a suitable
# performance. The following is a suitable set of arrangements.
#----------------------------------------------------------------
solver_minimization_parameters =  {"method" : "gpcg", 
                                    "linear_solver" : "gmres",
                            #--------------------------------
                            # These are parameters for optimization
                            #--------------------------------
                            "line_search": "armijo",
                            #"preconditioner" : "bjacobi", 
                            #"preconditioner" : "jacobi", 
                            "preconditioner" : "hypre_euclid", 
                            "maximum_iterations" :200,  
                            "error_on_nonconvergence": False,
                            #--------------------------------
                            # These are parameters for linear solver
                            #--------------------------------
                            "krylov_solver" : {
                                "maximum_iterations" : 200,
                                "nonzero_initial_guess" : True,
                                "report" : True,
                                "monitor_convergence" : False,
                                "relative_tolerance" : 1e-8
                            }
                            #--------------------------------
                           }


#----------------------------------------------------------------
# The linear solver requires parameters to get a suitable
# performance. The following is a suitable set of arrangements.
#----------------------------------------------------------------
solver_LS_parameters =  {"linear_solver" : "cg", #"mumps",
                            "symmetric" : True, 
                            #"preconditioner" : "bjacobi", 
                            "preconditioner" : "jacobi", # ivan
                            "krylov_solver" : {
                                "report" : True,
                                "monitor_convergence" : False,
                                "relative_tolerance" : 1e-8
                                }
                            }
solver_LS2_parameters =  {"krylov_solver" : {
                "nonzero_initial_guess" : True
}}


#------------------------------------------------------
# In this block of code define the operators.  These is
# independent from the mesh.
#------------------------------------------------------
# Constitutive functions of the damage model. Here
# we define the operators acting on the damage system
# as well as the operator acting on the displacement
# field, which depends on the damage.
#------------------------------------------------------
def w(alpha):
    if modelo=="lin":
        return w11*alpha
    if modelo =="quad":
        return w11*alpha**2
    
def A(alpha):
    return (1-alpha)**2
   
#-------------------------------------------------------------------------------
#  Define th Deviator and Spheric Tensors
#-------------------------------------------------------------------------------
def Dev(Tensor):
    return Tensor-Sph(Tensor)

def Sph(Tensor):
    Tensor2=as_matrix([[1,0],[0,1]])
    return (inner(Tensor,Tensor2)/inner(Tensor2,Tensor2))*Tensor2    
 
#------------------------------------------------------
# Strain and stress in free damage regime.
#------------------------------------------------------
def eps ( v):
    return sym ( grad ( v) )
    
def sigma_0 ( eps):
    return 2.0 * mu * ( eps) + lmbda * tr (eps ) * Identity ( ndim)

#-------------------------------------------------------------------------------
# Fenics forms for the energies. This is affected by the damage in terms of the
# modification of the Young modulus. As this relationship is linear, we can write it as follows.
#-------------------------------------------------------------------------------
def sigma ( eps, alpha):
    return ( A ( alpha) + k_ell ) * sigma_0 ( eps)

#------------------------------------------------------------------
#  Mesh without cavity.
#------------------------------------------------------------------
# The information concerning the mesh is filled in on the file
# "parameters.py".  This subroutine provides the following
# elements:
#
#   mesh       : name of the Computational domain
#   boundaries : physical boundaries and their labels.
#
#
#   bc_u       : Array of boundary conditions for "u".
#   bc_alpha   : Arrat of boundary conditions for "alpha". 
#
 
#----------------------------------------------------------------------------
# Variational formulation begins.
# This is done in TestParameters
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# Define the function, test and trial fields. We have two types of operators
# "u, du, v" are vectorial expressions. "alpha, dalpha, beta" are scalar.
#----------------------------------------------------------------------------
u   = Function      ( V_vector, name = "u")
du  = TrialFunction ( V_vector)
v   = TestFunction  ( V_vector)

u0  = Function(V_vector, name="u0")
du0 = TrialFunction(V_vector)
v0  = TestFunction(V_vector)

alpha   = Function      ( V_scalar, name = "alpha")
alpha0  = Function      ( V_scalar, name = "alpha")
dalpha  = TrialFunction ( V_scalar)
beta    = TestFunction  ( V_scalar)

u_aux   = Function ( V_scalar)

W_energy        = Function ( V_scalar, name = "energy_w" )
dev_energy      = Function ( V_scalar, name = "energy_dev" )
div_energy      = Function ( V_scalar, name = "energy_sph" )
E_energy        = Function ( V_scalar, name = "energy_E" )
p_energy      	= Function ( V_scalar, name = "energy_p" )
dis_energy      = Function ( V_scalar, name = "energy_dis" )

stressG = Function( V_tensor, name = "sigma")
strainG = Function( V_tensor, name = "epsilon")

alphaAux    = Function ( V_scalar, name = "alpha")

# Define the function "alpha_error" to measure relative error, used in
alpha_error = Function ( V_scalar)

#----------------------------------------------------------------------------
# Interpolate the initial condition for the damage variable "alpha."
# It uses interpolation with degree = 0, it is because the
# initial condition is a constant. In the case of more general
# initial conditions the degree have to be at least two times
# larger than the degree chosen for solving the variational problems.
#----------------------------------------------------------------------------
alpha_0 =   interpolate ( Expression("0.",degree = 2), V_scalar)

#-----------------------------------------s-------------------------------------
# Let us define ds and dx 
#------------------------------------------------------------------------------
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
dx = Measure('dx', domain=mesh)

#------------------------------------------------------------------------------
# Let us define the total energy of the system as the sum of elastic energy,
# dissipated energy due to the damage and external work due to body forces. 
#------------------------------------------------------------------------------
if case == "Marigo":
    elastic_energy1 = 0.5*inner(sigma(eps(u), alpha), eps(u))*dx
    elastic_energy2 = 0.5*inner(sigma(eps(u), alpha), eps(u))*dx
if case == "DevSph": 
    elastic_energy1    = 0.5*inner(sigma(eps(u), alpha), eps(u))*dx
    elastic_energy2    = 0.5/E*(inner(Dev( sigma(eps(u),alpha) ), Dev( sigma(eps(u),alpha)  )) \
                            - 2.0/3.0*kappa2*inner(Sph( sigma(eps(u),alpha)) , Sph( sigma(eps(u),alpha)  )) )*dx  

external_work     = dot ( body_force, u) * dx
              
dissipated_energy = ( w ( alpha) +  ellv**2 * w1* dot ( grad ( alpha), grad ( alpha) ) ) * dx

total_energy1 = elastic_energy1 + dissipated_energy - external_work 
total_energy2 = elastic_energy2 + dissipated_energy - external_work

#-----------------------------------------------------------------------
# Weak form of elasticity problem. This is the formal expression
# for the tangent problem which gives us the equilibrium equations.
#-----------------------------------------------------------------------
E_u     = derivative ( total_energy1, u, v)
E_alpha = derivative ( total_energy2, alpha, beta)

# Hessian matrix
E_alpha_alpha = derivative ( E_alpha, alpha, dalpha)

# Writing tangent problems in term of test and trial functions for
# matrix assembly
E_du     = replace ( E_u,{u:du} )
E_dalpha = replace ( E_alpha, { alpha : dalpha} )

#------------------------------------------------------------------------------
# Once the tangent problems are formulated in terms of trial and text functions,
# we define the variatonal problems.
#------------------------------------------------------------------------------
# Variational problem for the displacement.
problem_u   = LinearVariationalProblem( lhs(E_du), rhs(E_du), u, bc_u)

# Define the classs Optimization Problem for then define the damage.
# Variational problem for the damage (non-linear to use variational
# inequality solvers of petsc)
class DamageProblem ( OptimisationProblem):   
    def __init__(self): # ivan
        OptimisationProblem.__init__(self)  
    # Objective vector
    def f(self, x):
        alpha.vector()[:] = x
        return assemble(total_energy2) 
    # Gradient of the objective function
    def F(self, b, x):
        alpha.vector()[:] = x
        assemble(E_alpha, tensor=b)
    # Hessian of the objective function
    def J(self, A, x):
        alpha.vector()[:] = x
        assemble(E_alpha_alpha, tensor=A)

# define the minimization problem using the class.
problem_alpha = DamageProblem()

#----------------------------------------------------------------------------
# Set up the solvers. Define the object for solving the displacement
# problem, "solver_u".
#----------------------------------------------------------------------------
solver_u    = LinearVariationalSolver(problem_u)

# Get the set of paramters for the class "solver_u".
# This only requires the solution of linear system solver.
solver_u.parameters.update(solver_LS_parameters)

#---------------------------------------------------------------
# we need to define the corresponding object, "solver_alpha".
#---------------------------------------------------------------
# The object associated to minimization is created.
solver_alpha = PETScTAOSolver ( )

# Get the set of paramters for the class "solver_alpha".
# This requires the solution of a minimization problem.
solver_alpha.parameters.update ( solver_minimization_parameters)

# As the optimization is a constrained type we need to provide the
# corresponding lower and upper  bounds.
lb = interpolate ( Expression ( "0.0", degree = 0), V_scalar)
ub = interpolate ( Expression ( "0.95", degree = 0), V_scalar)
lb.vector()[:] = alpha.vector()

#-----------------------------------------------------------------------
# Define extras.
#-----------------------------------------------------------------------
# Crete the files to store the solution of damage and displacements.
# use .pvd if .xdmf is not working
file_alpha      = File ( savedir + "/alpha.pvd")
file_u          = File ( savedir + "/u.pvd")

#-----------------------------------------------------------------------
#   Main loop.
#-----------------------------------------------------------------------
# Apply BC to the lower and upper bounds
# As the constraints may change we have to
# apply the boundary condition at any interation loop.
#for bc in bc_alpha :
#    bc.apply ( lb.vector ( ) )
#    bc.apply ( ub.vector ( ) )

#----------------------------        
# Alternate mininimization
#----------------------------
# Initialization
iter = 1; err_alpha = 1; err_alpha_aux=1

a0 = Vector(MPI.COMM_SELF)
a1 = Vector(MPI.COMM_SELF)
a2 = Vector(MPI.COMM_SELF)

#-------------------------------------------------------------------
# Iterations of the alternate minimization stop if an error limit is
# reached or a maximim number of iterations have been done.
#-------------------------------------------------------------------
count_arreglo=0
while True and err_alpha > toll and iter < maxiter :

    alpha.vector().gather(a0, np.array(range(V_scalar.dim()), "intc"))
    amin=alpha.vector().min()
    amax=alpha.vector().max()
    if MPI.COMM_WORLD.Get_rank() == 0:
        print("Job %d: Iteration:  %2d, a0:[%.8g,%.8g]"%(MPI.COMM_WORLD.Get_rank(),iter, a0.min(), a0.max()))

    # solve elastic problem
    solver_u.solve ( )
                
    err_alpha2=1
    internal_iter =1
    while err_alpha2>toll and internal_iter<maxiter:
        alpha.vector().gather(a1, np.array(range(V_scalar.dim()), "intc"))
        if MPI.COMM_WORLD.Get_rank() >= 0:
            print("Job %d: itmesh=%-2d, Iteration:  %2d.%-2d, a1:[%.8g,%.8g]" \
                     %(MPI.COMM_WORLD.Get_rank(),0,iter,internal_iter, a1.min(), a1.max()))
        # solve damage problem via a constrained minimization algorithm.
        solver_alpha.solve ( problem_alpha, alpha.vector ( ), lb.vector ( ), ub.vector ( ) )
        alpha.vector().get_local()[alpha.vector().get_local( ) > 0.95] = 0.95
        alpha.vector().gather(a2, np.array(range(V_scalar.dim()), "intc"))
        
        err_alpha2 = np.linalg.norm(a2 - a1, ord = np.Inf)
        
        if MPI.COMM_WORLD.Get_rank() == 0:
            print("Job %d: Internal_Iteration:  %2d,            aError: %2.8g, alpha_max: %.8g,   [%.8g,%.8g]" \
                    %(MPI.COMM_WORLD.Get_rank(),internal_iter, err_alpha2, a2.max(), (a2-a0).min(), (a2-a0).max()))
        internal_iter = internal_iter +1

    # Compute the norm of the the error vector.
    err_alpha = np.linalg.norm(a2 - a0, ord = np.Inf) #np.linalg.norm ( alpha_error.vector ( ).get_local ( ),ord = np.Inf)
    
    count_arreglo_int=0
    if C_L != 0.0:
        while err_alpha > err_alpha_aux:
            count_arreglo=count_arreglo+1
            count_arreglo_int=count_arreglo_int+1
            print("-----------------------------------")
            print(" ARREGLO DE ALPHA NUMBER: %2d.%-2d " %(iter,count_arreglo_int))
            print(" ARREGLO DE ALPHA NUMBER TOTAL: %d " %(count_arreglo_int))
            print("-----------------------------------")
            alpha_2 = C_L*alpha_0+(1.0-C_L)*alpha
            alpha.assign(alpha_2)  
            alpha.vector().gather(a2, np.array(range(V_scalar.dim()), "intc"))            
            err_alpha = np.linalg.norm(a2 - a0, ord = np.Inf)  

    # monitor the results
    if MPI.COMM_WORLD.Get_rank() >= 0:
        print ("Iteration:  %2d, Error: %2.8g, alpha_max: %.8g" % ( iter, err_alpha, alpha.vector ( ).max ( ) ))

    # update the solution for the current alternate minimization iteration.
    alpha_0.assign(alpha)

    iter = iter + 1
#End of   "while err_alpha > toll and iter < maxiter"

# updating the lower bound with the solution of the solution
# corresponding to the current global iteration, it is for accounting
# for the irreversibility.
lb.vector ( ) [ :] = alpha.vector ( )

print ("------------------------------------")
print (" End of the alternate minimization. ")
print ("------------------------------------")

#---------------------------------------------------------------------------
# End of Main loop.
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
# Post-Jobing after having calculated u and alpha
#---------------------------------------------------------------------------
# Store u,alpha, sigma and epsilon
if MPI.COMM_WORLD.Get_rank() >= 0:
    file_u         << ( u, 0.)
    file_alpha     << ( alpha   , 0.)

# plot the damage fied
#plot ( alpha, range_min = 0., range_max = 1., key = "alpha",title = "Damage at Mesh %s"%(0))
#plt.savefig(savedir +"/alpha""_"+str(0)+".png")                
# Store the damage for this geometry
alphaAux.assign ( alpha)
alpha0.assign ( alpha)

#------------------------------------------------------------------
# The main loop for mesh without cavity  has finished.
#------------------------------------------------------------------
print ("-------------------------------------------------------------------")
print ("              Geometry without cavity is finished.                 ")
print ("-------------------------------------------------------------------")

# Remove previous integrating factors "dx, ds"
del ds, dx

#------------------------------------------------------------------
#  Mesh with cavity.
#------------------------------------------------------------------
# Start loop over new geometries. These are obtained from a sequence
# of geometries which are obtained from an external folder. 
# The number of external files is "NstepW" and the call is driven
# by the counter "itmesh".
fDatos=  open(savedir + '/Parametros.txt', 'w')
buff='Case       =%s\n'%(case);fDatos.write(buff)     ; print(buff)
buff='Modelo     =%s\n'%(modelo);fDatos.write(buff)     ; print(buff)
buff='E          =%f\n'%(E);fDatos.write(buff)     ; print (buff)
buff='w1         =%f\n'%(w1);fDatos.write(buff)     ; print (buff)
buff='ellv       =%f\n'%(ellv);fDatos.write(buff)   ; print (buff)
buff='ell        =%f\n'%(ell);fDatos.write(buff)     ; print (buff)
fDatos.close()


#---------------------------------------------------------------------------------------
#  Starting the loop of the mesh sequence. It is driven by the index "itmesh".
#---------------------------------------------------------------------------------------
while itmesh <= NstepW :
    a0 = Vector(MPI.COMM_SELF)
    a1 = Vector(MPI.COMM_SELF)
    a2 = Vector(MPI.COMM_SELF)
    
    class Structure(SubDomain):
        def inside(self, x, on_boundary):
            return between(x[0] ,(-500.0,-500+40*itmesh)) and between(x[1] ,(-20.0,20.0)) #ejemplo pequeño
            #return between(x[0] ,(0.0,20*itmesh)) and between(x[1] ,(0.0,12.0)) #ejemplo grande
            
    # Create sub domain markers and mark everaything as 0
    sub_domains = MeshFunction("size_t", mesh, mesh.topology().dim())
    sub_domains.set_all(0)

    # Mark structure domain as 1
    structure = Structure()
    structure.mark(sub_domains, 1)

    # Extract sub meshes
    domain_new = SubMesh(mesh, sub_domains, 0)
    tunel_new = SubMesh(mesh, sub_domains, 1)

    
    # Define boundary sets for boundary conditions
    class Left_new(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0], -1500.)  #ejemplo pequeño 
            #return near(x[0], -1500.) #ejemplo grande   
    class Right_new(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[0] , 1500.)
            #return near(x[0] , 2300.) 
    class Top_new(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], 500.) 
            #return near(x[1], 500.)     
    class Bottom_new(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], -500.)
            #return near(x[1], -500.)
    
            

    # Initialize sub-domain instances
    left_new        = Left_new() 
    right_new       = Right_new()
    top_new         = Top_new() 
    bottom_new      = Bottom_new()
    
    # define meshfunction to identify boundaries by numbers
    boundaries_new = MeshFunction("size_t", domain_new, domain_new.topology().dim()-1)
    boundaries_new.set_all(0)
    left_new.mark  (boundaries_new, 1) # mark left as 1
    right_new.mark (boundaries_new, 2) # mark right as 2
    top_new.mark   (boundaries_new, 3) # mark top as 3
    bottom_new.mark(boundaries_new, 4) # mark bottom as 4 
   
    
    dsN = Measure('ds', domain=domain_new, subdomain_data=boundaries_new)
    dxN = Measure('dx', domain=domain_new)
    
    #normal vectors
    normal_v_new = FacetNormal ( domain_new)
    
    # Create new function space for elasticity + Damage
    V_vector_new   = VectorFunctionSpace ( domain_new, "CG", 1)
    V_scalar_new   = FunctionSpace       ( domain_new, "CG", 1)
    V_tensor_new   = TensorFunctionSpace ( domain_new, "DG", 0)
    

    #--------------------------------------------------------------------------------------
    # Observation: To generate a sequence of plots in paraview the name
    # of the variable must be the same. It is achieved by including name="alpha".
    # at the moment of the definition of the structure "alpha".
    #
    #  << alphaN =  Function ( V_scalar_new, name="alpha") >>
    #
    # The same definition needs to be done for displacement "u" and
    # other arrays as the difference of damage without cavity and damage with cavity,
    # "alphaDiff".
    #-------------------------------------------------------------------------------------
    # Define the function, test and trial fields
    uN, duN, vN = Function ( V_vector_new, name="u"), \
                  TrialFunction ( V_vector_new), TestFunction ( V_vector_new)

    alphaN,alphaN_2, dalphaN, betaN = Function ( V_scalar_new, name="alpha"), Function ( V_scalar_new, name="alpha"), \
                             TrialFunction ( V_scalar_new), \
                             TestFunction(V_scalar_new)

    #alpharef  = Function ( V_scalar_new, name = "alpha")
    
    # name = "alpha_diff" forces the output format to rename the variables with this name.
    #alphaDiff = Function ( V_scalar_new, name = "alpha_diff")

    #-------------------------------------------------
    # Project the rerence solution into the new mesh.
    # Warning: In parallel implementations this projection
    # may fail.
    #-------------------------------------------------    
    #alpharef    = interpolate (  alpha, V_scalar_new)
    #u0ref       = interpolate (u0, V_vector_new)
    #alpha0ref   = interpolate (alpha0, V_scalar_new)
    alphaN_0  = Function ( V_scalar_new, name = "alpha")
    # Define the initial damage.
    #alphaN_0 = interpolate ( Expression ( "0.0", degree = 1), V_scalar_new) 
 
    alphaN_0    = interpolate (  alphaAux, V_scalar_new)


    amin=alphaN.vector().min()  
    amax=alphaN.vector().max()
    asize=alphaN.vector().size()

    if MPI.COMM_WORLD.Get_rank() <=0:
        print("----------------------------------------------------------------------------------------------------------------") 
    if MPI.COMM_WORLD.Get_rank() <= 0:
        print("Job %d: itmesh=%-2d, alphaN  :[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize, currentframe().f_lineno))
    
    amin=alphaN_0.vector().min()
    amax=alphaN_0.vector().max()
    asize=alphaN_0.vector().size()

    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaN_0:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))
    
    amin=alphaAux.vector().min()
    amax=alphaAux.vector().max()
    asize=alphaAux.vector().size()

    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaAux:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))  
       


    alphaN_0.vector().get_local( )[alphaN_0.vector().get_local( ) < 1e-12] = 0.0
    alphaN_0.vector().get_local( )[alphaN_0.vector().get_local( ) > 0.95] = 0.95
    alphaN.assign(interpolate ( alphaN_0, V_scalar_new))
    alphaN.vector().get_local( )[alphaN_0.vector().get_local( ) < 1e-12] = 0.0
    alphaN.vector().get_local( )[alphaN_0.vector().get_local( ) > 0.95] = 0.95   

    if MPI.COMM_WORLD.Get_rank() <=0:
       print("----------------------------------------------------------------------------------------------------------------") 
    amin=alphaN.vector().min()
    amax=alphaN.vector().max()
    asize=alphaN.vector().size()
    if MPI.COMM_WORLD.Get_rank() <= 0:
        print("Job %d: itmesh=%-2d, alphaN  :[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize, currentframe().f_lineno))
    
    amin=alphaN_0.vector().min()
    amax=alphaN_0.vector().max()
    asize=alphaN_0.vector().size()
    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaN_0:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))
    
    amin=alphaAux.vector().min()
    amax=alphaAux.vector().max()
    asize=alphaAux.vector().size()
    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaAux:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))
    
    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("----------------------------------------------------------------------------------------------------------------") 
    
    #----------------------------------------------------------------------------
    # Boudary conditions. Definiton of Dirichlet as well as homogeneous Neumann.
    # Notice that homegeneous Neumann b.c. are equivalent to omite any infomation
    # at boundaries.
    #----------------------------------------------------------------------------
    # Brief description about the set of boundary conditions for the displacement
    # field (Optional).
    #----------------------------------------------------------------------------
    
    #-----------------------------------------
    # Face Corner Bottom for the uniqueness
    #-----------------------------------------
    zero_v_new = Constant((0.,)*ndim)
    u_0_new = zero_v_new


    bc_boxbottomN = DirichletBC ( V_vector_new, u_0_new, boundaries_new, 4)
    bc_leftN = DirichletBC ( V_vector_new.sub(0), 0.0, boundaries_new, 1)
    bc_rightN = DirichletBC ( V_vector_new.sub(0), 0.0, boundaries_new, 2)
    bc_uN = [bc_boxbottomN, bc_leftN, bc_rightN]
        
    # Boundary condition for the damage is damped into the array "bc_alphaN".
    #bc_alpha_upN = DirichletBC ( V_scalar_new, 0.0, boundaries_new, CAVEUP )
    #bc_alpha_bottomN = DirichletBC ( V_scalar_new, 0.0, boundaries_new, CAVEBOTTOM )
    #bc_alpha_midN = DirichletBC ( V_scalar_new, 0.0, boundaries_new, CAVEMID )
    # bc - alpha (Neumann boundary condition for damage on the left boundary)
    #bc_alphaN = [ ]
    
    #--------------------------------------------------------------------------------        
    # Let us define the total energy of the system as the sum of elastic energy,
    # dissipated energy due to the damage and external work due to body forces. 
    #--------------------------------------------------------------------------------
    if case == "Marigo":
        elastic_energy1_new = 0.5*inner(sigma(eps(uN), alphaN), eps(uN))*dxN
        elastic_energy2_new = 0.5*inner(sigma(eps(uN), alphaN), eps(uN))*dxN
    if case=="DevSph":
        elastic_energy1_new    = 0.5*inner(sigma(eps(uN), alphaN), eps(uN))*dxN
        elastic_energy2_new   = 0.5/E*(inner(Dev( sigma(eps(uN),alphaN) ), Dev( sigma(eps(uN),alphaN)  )) \
                                    - 2.0/3.0*kappa2*inner(Sph( sigma(eps(uN),alphaN)) , Sph( sigma(eps(uN),alphaN)  )) )*dxN  
    
    external_work_new      = dot (body_force, uN) * dxN
              

    dissipated_energy_new = ( w ( alphaN) + ellv**2 *w1 * dot ( grad ( alphaN), grad ( alphaN) ) ) * dxN

    total_energy1_new = elastic_energy1_new + dissipated_energy_new-external_work_new 
    total_energy2_new = elastic_energy2_new + dissipated_energy_new-external_work_new 
     

    #--------------------------------------------------------------------------------            
    # Weak form of elasticity problem. This is the formal expression
    # for the tangent problem which gives us the equilibrium equations.
    #-------------------------------------------------------------------------------- 
    E_uN     = derivative ( total_energy1_new, uN, vN)
    E_alphaN = derivative ( total_energy2_new, alphaN, betaN)
    
    # Hessian matrix
    E_alpha_alphaN = derivative ( E_alphaN, alphaN, dalphaN)
    
    # Writing tangent problems in term of test and trial functions for matrix assembly
    E_duN     = replace ( E_uN, { uN : duN} )
    E_dalphaN = replace ( E_alphaN, { alphaN : dalphaN} )

    #---------------------------------------------------------------------------------        
    # Once the tangent problems are formulated in terms of trial and text functions,
    # we define the variatonal problems.
    #---------------------------------------------------------------------------------
    # Variational problem for the displacement.
    problem_uN     = LinearVariationalProblem ( lhs ( E_duN), rhs ( E_duN), uN, bc_uN)
    
    #------------------------------------------------------------------------------------
    # For solve the first order equilibrium, we need to create the variational formulation
    # from the tangent problem "E_alpha".
    #------------------------------------------------------------------------------------      
    class DamageProblemN ( OptimisationProblem) :
        def __init__ ( self): 
            OptimisationProblem.__init__ ( self) 
        # Objective vector    
        def f ( self, x):
            alphaN.vector ( ) [ :] = x
            return assemble ( total_energy2_new)
        # Gradient of the objective function
        def F ( self, b, x):
            alphaN.vector ( ) [ :] = x
            assemble ( E_alphaN, tensor = b)                       
        # Hessian of the objective function    
        def J ( self, A, x):
            alphaN.vector ( ) [ :] = x
            assemble ( E_alpha_alphaN, tensor = A)
               
    # The following line activates the optimization solver for the damage.    
    problem_alphaN = DamageProblemN ( )

    #-----------------------------------------------------------------------------
    # Set up the solvers       
    #-----------------------------------------------------------------------------                                 
    solver_uN = LinearVariationalSolver ( problem_uN)
    solver_uN.parameters.update ( solver_LS_parameters)
        
    solver_alphaN = PETScTAOSolver ( )
    solver_alphaN.parameters.update ( solver_minimization_parameters)
            
    #--------------------------------------------------------------------------------
    #  For the constraint minimization problem we require the lower and upper bound,
    # "lbN" and "ubN".  They are initialized though interpolations.
    #--------------------------------------------------------------------------------
    lbN = alphaN_0
    ubN = interpolate ( Expression ( "9.5", degree = 0), V_scalar_new) 

    #---------------------------------------------------------------------------------                                
    # Define the function "alpha_error" to measure relative error, used in
    # stop criterion.
    #---------------------------------------------------------------------------------
    alphaN_error = Function ( V_scalar_new)
    
    #---------------------------------------------------------------------------------                                
    #   Main loop.
    #---------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------                                
    # Apply BC to the lower and upper bounds
    # As the constraints may change we have to
    # apply the boundary condition at any interation loop.
    #-----------------------------------------------------------------------------
    #for bc in bc_alphaN :
    #bc.apply ( lbN.vector ( ) )
    #bc.apply ( ubN.vector ( ) )
     
    #-----------------------------------------------------------------------------                                        
    # Alternate mininimization
    # Initialization                
    #-----------------------------------------------------------------------------          
    iter = 1; err_alphaN = 1; err_alpha_aux=1
        
    #-------------------------------------------------------------------    
    # Iterations of the alternate minimization stop if an error limit is
    # reached or a maximim number of iterations have been done.
    #-------------------------------------------------------------------
    count_arreglo=0
    while err_alphaN > toll and iter < maxiter :
        alphaN.vector().gather(a0, np.array(range(V_scalar_new.dim()), "intc"))

        amin=alphaN.vector().min()
        amax=alphaN.vector().max()
        if MPI.COMM_WORLD.Get_rank() == 0:
            print("Job %d: itmesh=%-2d, Iteration:  %2d   , a0:[%.8g,%.8g], alphaN:[%.8g,%.8g]" \
                    %(MPI.COMM_WORLD.Get_rank(),itmesh,iter, a0.min(), a0.max(), amin, amax))
        
        # solve elastic problem
        solver_uN.solve ( )
        
        err_alphaN2=1
        internal_iter =1
        while err_alphaN2>toll and internal_iter<maxiter:
            alphaN.vector().gather(a1, np.array(range(V_scalar_new.dim()), "intc"))
            if MPI.COMM_WORLD.Get_rank() >= 0:
                print("Job %d: itmesh=%-2d, Iteration:  %2d.%-2d, a1:[%.8g,%.8g]" \
                        %(MPI.COMM_WORLD.Get_rank(),itmesh,iter,internal_iter, a1.min(), a1.max()))
            # solve damage problem via a constrained minimization algorithm.
            solver_alphaN.solve ( problem_alphaN, alphaN.vector ( ), lbN.vector ( ), ubN.vector ( ) )
            alphaN.vector().get_local( )[alphaN.vector().get_local( ) > 0.95] = 0.95
            alphaN.vector().gather(a2, np.array(range(V_scalar_new.dim()), "intc"))
            
            err_alphaN2 = np.linalg.norm(a2 - a1, ord = np.Inf)

            if MPI.COMM_WORLD.Get_rank() >= 0:
                print("Job %d: itmesh=%-2d, Iteration:  %2d,             err_alphaN2=%2.8g, alpha_max: %.8g,   da=[%.8g,%.8g], toll=%2.8g" \
                        %(MPI.COMM_WORLD.Get_rank(),itmesh,iter, err_alphaN2, a2.max(), (a2-a1).min(), (a2-a1).max(),toll))
            internal_iter = internal_iter +1

        err_alphaN = np.linalg.norm(a2 - a0, ord = np.Inf)

        count_arreglo_int=0
        if C_L != 0.0:
            while err_alphaN > err_alpha_aux:
                count_arreglo=count_arreglo+1
                count_arreglo_int=count_arreglo_int+1
                print("-----------------------------------")
                print(" ARREGLO DE ALPHA NUMBER: %2d.%-2d " %(iter,count_arreglo_int))
                print(" ARREGLO DE ALPHA NUMBER TOTAL: %d " %(count_arreglo_int))
                print("-----------------------------------")
                alphaN_2 = C_L*alphaN_0+(1.0-C_L)*alphaN
                alphaN.assign(alphaN_2)  
                alphaN.vector().gather(a2, np.array(range(V_scalar_new.dim()), "intc"))            
                err_alphaN = np.linalg.norm(a2 - a0, ord = np.Inf)  

        # monitor the results for the new mesh
        if MPI.COMM_WORLD.Get_rank() >= 0:
            print ("Job %d: itmesh=%-2d, Iteration:  %2d   , Error: %2.8g, alpha_max: %.8g" \
                    % ( MPI.COMM_WORLD.Get_rank(),itmesh, iter, err_alphaN, alphaN.vector ( ).max ( ) ))
        
        # update the solution for the current alternate minimization iteration.
        err_alpha_aux = err_alphaN
        alphaN_0.assign ( alphaN)
                    
        iter = iter + 1
                                                                                                                    
    if MPI.COMM_WORLD.Get_rank() >= 0:
        print ("-------------------------------------------------------------------")
        print ("    End of the alternate minimization in Remesh: %d    " %( itmesh ))
        print ("-------------------------------------------------------------------")

    #----------------------------------------------------------------------------------------
    # Once a new damage has been obtained, we store it into an auxiliary variable "alphaAux"
    #----------------------------------------------------------------------------------------
    alphaAux = Function ( V_scalar_new, name = "alpha")
    alphaAux.assign ( alphaN)
    #  Control if alpha is too small
    #alphaN.vector()[alphaN.vector() < 1e-12] = 0.0
    
    
    #JSMInterpola_In_Out(alphaN, alphaAux, GVertex0_2_GVertexI) 
             
    amin=alphaN.vector().min()
    amax=alphaN.vector().max()
    asize=alphaN.vector().size()
    if MPI.COMM_WORLD.Get_rank() <= 0:
        print("Job %d: itmesh=%-2d, alphaN  :[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize, currentframe().f_lineno))
    
    amin=alphaN_0.vector().min()
    amax=alphaN_0.vector().max()
    asize=alphaN_0.vector().size()
    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaN_0:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))
    
    amin=alphaAux.vector().min()
    amax=alphaAux.vector().max()
    asize=alphaAux.vector().size()
    if MPI.COMM_WORLD.Get_rank()  <= 0:
        print("Job %d: itmesh=%-2d, alphaAux:[%17.8g,%17.8g] size=%d (l=%d)" \
              %(MPI.COMM_WORLD.Get_rank(),itmesh, amin, amax,asize,currentframe().f_lineno))
    
    #-----------------------------------------------------------------------------------                                                                                            
    # Dump the solution into files.
    # In order to evidence the influence of the cavity we
    # take the difference between the reference solution (without holes)
    # and the solution corresponding to the augmented cylinder.
    #-----------------------------------------------------------------------------------
    #alphaDiff.vector()[:] = alphaN.vector() - alpha0ref.vector()
    #alphaDiff.vector()[alphaDiff.vector() < 0] = 0.0
    
    
    if MPI.COMM_WORLD.Get_rank() >= 0:
        file_u         << ( uN, 1.0 * itmesh)
        file_alpha     << ( alphaN   , 1.0 * itmesh)

    # plot the damage fied
    #plot ( alphaN, range_min = 0., range_max = 1., key = "alpha",title = "Damage at Mesh %s"%(itmesh))
    #plt.savefig(savedir +"/alpha"+"_"+str(itmesh)+".png")
    
    itmesh = itmesh + 1

    #------------------------------------------------------------------------------------   
    # Free memory for lists depending on the current mesh iteration
    #------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------
    # Free memory for function, test functions and general arrays
    # involved into algebraic manipulations.
    #---------------------------------------------------------------------------------
    #del uN
    del duN
    del vN
        
    del alphaN
    del dalphaN
    del betaN
        
    del alphaN_0
    #del alpharef
    del alphaN_error
    #del alphaDiff


        
    del ubN
    del lbN
        
    #---------------------------------------------------------------------------------        
    # Free memory for boundary conditions
    # Please be carefull with the name of variables.
    #---------------------------------------------------------------------------------
    del bc_uN


    del normal_v_new
        
    #---------------------------------------------------------------------------------
    # Free the memory for geometry
    #---------------------------------------------------------------------------------
    del boundaries_new
    del domain_new
        
    del V_vector_new
    del V_scalar_new
        
    #---------------------------------------------------------------------------------      
    # Free the memory for solvers.
    #---------------------------------------------------------------------------------
    del total_energy1_new
    del elastic_energy1_new
    del total_energy2_new
    del elastic_energy2_new
    del external_work_new
    del dissipated_energy_new

    del E_uN
    del E_alphaN
    del E_alpha_alphaN

    del E_duN
    del E_dalphaN
        
    del solver_uN
    del problem_uN
    del problem_alphaN

    del dsN, dxN

    del DamageProblemN
    del a0
    del a1
    del a2

#---------------------------------------------------------------------------------
# The main loop for remeshing has finished.
#---------------------------------------------------------------------------------

print("-------------------------------------------------------------------")
print("                Geometry with cavity is finished.                  ")
print("-------------------------------------------------------------------")


#------------------------------------------------------------------------------------ 
# Free Memory
#------------------------------------------------------------------------------------ 
#del energies

del u, du, v

del alpha, dalpha, beta, alpha_0
del alpha_error


del file_alpha
del file_u


del lb, ub

del solver_u
del solver_alpha
del alphaAux
del V_vector
del V_scalar




del normal_v
del mesh, boundaries

#----------------------------------------------------------------
#   End of the main program.
#----------------------------------------------------------------