#-----------------------------------------------
#  Import libraries to get the code working.
#-----------------------------------------------
from dolfin import *
from mshr import *
import sys, os, sympy, shutil, math
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt 
import socket
import datetime
from ufl import replace
from mpi4py import MPI
from inspect import currentframe, getframeinfo,stack
#----------------------------------------------------------------
# MESH  
#----------------------------------------------------------------
#mesh = RectangleMesh(Point(-1500.0, -500.0), Point(2300.0, 500.0), 950, 250, "crossed") #itmesh=40
mesh = RectangleMesh(Point(-1500.0, -500.0), Point(1500.0, 500.0), 750, 250, "crossed") #itmesh=25
#----------------------------------------------------------------
# PARAMETERS    
#----------------------------------------------------------------
itmesh = 1
NstepW = 25 #30 #40 #10

Scale=1.0
modelo="lin"
w1=1.0e4
w11=1.0e4

C_L = 0.9

kappa2 =1.0
case="DevSph" #"Marigo";"DevSph"


#------------------------------------------------------
# Material constant. The values of parameters involved
# in the models are defined here.
#------------------------------------------------------
E  = 29e9
nu = 0.3



mu    = E / ( 2.0 * ( 1.0 + nu))
lmbda = E * nu / ( 1.0 - nu**2)

# In this case this quantity contains the density, so
# gravity =  rho * g, with g the gravity acceleration.
rho    = 2.7e3
g    = 9.8
gravity = rho*g

k_ell = Constant(1.e-6) #residual stiffness

body_force = Constant( (0.0, -gravity) )


h=CellDiameter(mesh) # diameters of all elements
hmin=mesh.hmin() # minimum of all diameters
hmax=mesh.hmax() # maximun of all diameters
ellv = 5.0*hmin    #2.0*hmin #5.0*hmin #0.5*hmin
ell=ellv/sqrt(2.0)

#------------------------------------------------------
# Numerical parameters of the alternate minimization.
# The algorithm stops when it reaches "maxiter" iterations,
# or alternatively it stops when the error between two
# sucessive iterations reaches the tolerance "toll".
#------------------------------------------------------
maxiter = 1e3
toll    = 1e-5

#-------------------------------------------------------------
# The output is stored in a folder with path especified
# in "savedir".
# The files are stored in a folder named "modelname".
#--------------------------------------------------------------
date=datetime.datetime.now().strftime("%m-%d-%y_%H.%M.%S")
where=socket.gethostname()
modelname = "[2]_[case=%s]_[modelo=%s]_[w1=%g]_[w11=%g]_[ell=%.2g]_[Date=%s]_[Where=%s]"%(case,modelo,w1,w11,ellv,date,where) 
#modelname = "[PRUEBA]_[case=%s]_[modelo=%s]_[w1=%g]_[w11=%g]_[ell=%.2g]"%(case,modelo,w1,w11,ellv) 
print('modelname='+modelname)

# others
regenerate_mesh = True
savedir = "results/%s"%(modelname)
print('savedir='+savedir)

#----------------------------------------------------------------
# Parameters to define the geometry if apply are defined here.
# Geometry
#----------------------------------------------------------------
ndim = mesh.geometry().dim() # get number of space dimensions


# Define boundary sets for boundary conditions
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], -1500.)  #ejemplo pequeño 
        #return near(x[0], -1500.) #ejemplo grande        
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0] , 1500.)
        #return near(x[0] , 2300.) 
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 500.) 
        #return near(x[1], 500.)     
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], -500.)
        #return near(x[1], -500.)

# Initialize sub-domain instances
left        = Left() 
right       = Right()
top         = Top() 
bottom      = Bottom()

# define meshfunction to identify boundaries by numbers
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
left.mark  (boundaries, 1) # mark left as 1
right.mark (boundaries, 2) # mark right as 2
top.mark   (boundaries, 3) # mark top as 3
bottom.mark(boundaries, 4) # mark bottom as 4


# Maximum values of mesh
meshl_xmax,meshl_ymax = mesh.coordinates().max(axis=0)

mesh_xmax = MPI.COMM_WORLD.allreduce(meshl_xmax, op=MPI.MAX)
mesh_ymax = MPI.COMM_WORLD.allreduce(meshl_ymax, op=MPI.MAX)


meshl_xmin,meshl_ymin = mesh.coordinates().min(axis=0)


mesh_xmin = MPI.COMM_WORLD.allreduce(meshl_xmin, op=MPI.MIN)
mesh_ymin = MPI.COMM_WORLD.allreduce(meshl_ymin, op=MPI.MIN)

print ("ZMAX(G,L):", mesh_ymax,meshl_ymax)
print ("ZMIN(G,L):", mesh_ymin,meshl_ymin)

# normal vectors
normal_v = FacetNormal(mesh)

#------------------------------------------------------
# Create function space for 3D elasticity + Damage
#------------------------------------------------------
V_vector     = VectorFunctionSpace ( mesh, "CG", 1)
V_scalar     = FunctionSpace       ( mesh, "CG", 1)
V_tensor     = TensorFunctionSpace ( mesh, "DG", 0)

#----------------------------------------------------------------------------
# Boudary conditions.
# Definiton of Dirichlet as well as homogeneous Neumann.
# Notice that homegeneous Neumann b.c. are equivalent to omite any infomation
# at boundaries.
#----------------------------------------------------------------------------
# Brief description about the set of boundary conditions for the displacement
# field.
#----------------------------------------------------------------------------
zero_v = Constant((0.,)*ndim)
u_0 = zero_v
bc_left = DirichletBC ( V_vector.sub(0), 0.0, boundaries, 1)
bc_right = DirichletBC (V_vector.sub(0),0.0,boundaries, 2)
bc_boxbottom = DirichletBC ( V_vector, u_0, boundaries, 4)
bc_u = [bc_boxbottom, bc_left, bc_right]